﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CountdownLetters
{
    public static class Lexicon
    {
        static string BasePath = System.AppDomain.CurrentDomain.BaseDirectory;
        static string DictionaryName = "resources/wordlist.txt";
        static string FilePath = BasePath + DictionaryName;
        static List<int> _lettersToRemove = new List<int>();

        static List<string> _words;

        public static void LoadResource()
        {
            try
            {
                _words = new List<string>();
                _words = File.ReadLines(FilePath).ToList();
                _words.Sort();
            }
            catch(Exception ex)
            {
                Console.WriteLine("Unable to load dictionry resource");
            }
        }

        public static bool CheckDictionaryForWord(string word)
        {
            return _words.BinarySearch(word) >= 0;
        }

        //Remove letters in char arrays to create letter sets
        private static List<string> RemoveLettersWithIndexes(List<List<int>> combinations, List<char> letters)
        {
            List<string> potentialLetters = new List<string>();

            foreach (var intCombination in combinations)
            {
                List<int> sortedComboList =  intCombination.Distinct().OrderByDescending(v => v).ToList();
                if(sortedComboList.Count < intCombination.Count)
                {
                    //prevent duplicates slowing down our processing.
                    continue;
                }

                List<char> lettersTemp = letters.Select(i => i).ToList();
                for (int inner = 0; inner < sortedComboList.Count; inner++)
                {
                    
                    lettersTemp.RemoveAt(sortedComboList[inner]);
                }
                string word = "";
                lettersTemp = lettersTemp.OrderBy(l => l).ToList();
                for (int i = 0; i < lettersTemp.Count; i++)
                {
                    word += lettersTemp[i];
                }
                potentialLetters.Add(word);
            }
            return potentialLetters;

        }

        //get the longest word available given a set of letters
        public static string GetLongestWordForletters(List<char> letters)
        {
            for (int i = 0; i < letters.Count; i++)
            {
                if(i > 0)
                {
                    List<List<int>> intCombinations = new List<List<int>>();
                    GetInputCombinations(letters, i, intCombinations);
                    List<string> letterCombinations = RemoveLettersWithIndexes(intCombinations, letters);

                    foreach (string word in letterCombinations.Distinct())
                    {
                        string match = CheckLexiconForPerm(word.ToCharArray().ToList());
                        if(match != null)
                        {
                            return match;
                        }
                    }
                }
                else
                {
                    string match = CheckLexiconForPerm(letters);
                    if (match != null)
                    {
                        return match;
                    }
                }

            }

            return "No words found";
        }

        private static string CheckLexiconForPerm(List<char> letters)
        {
            List<string> possibleWords = new List<string>();
            Permut(letters.ToArray(), 0, letters.Count, possibleWords);
            foreach (string word in possibleWords)
            {
                bool wordExists = CheckDictionaryForWord(word);
                if (wordExists)
                {
                    return word;
                }
            }
            return null;
        }

        public static List<List<int>> GetInputCombinations(List<char> letters, int numToRemove, List<List<int>> combintions)
        {
            for (int i = 0; i < letters.Count; i++)
            {
                _lettersToRemove.Add(i);
                if (_lettersToRemove.Count == numToRemove)
                {
                    List<int> ints = _lettersToRemove.Select(i => i).ToList();
                    combintions.Add(ints);
                    _lettersToRemove.RemoveAt(_lettersToRemove.Count - 1);
                }
                else
                {
                    GetInputCombinations(letters, numToRemove, combintions);
                }
                
                if(i+1 == letters.Count && _lettersToRemove.Count > 0)
                {
                    _lettersToRemove.RemoveAt(_lettersToRemove.Count - 1);
                }
            }
            return combintions;
        }

        //Workout possible strings for given letters
        public static void Permut(char[] list, int current, int max, List<string> possibleWords)
        {
            int i;
            if (current == max)
            {
                string word = "";
                for (i = 0; i < max; i++)
                {
                    word += list[i];
                }
                possibleWords.Add(word);
            }
            else
                for (i = current; i < max; i++)
                {
                    SwapTwoLetters(ref list[current], ref list[i]);
                    Permut(list, current + 1, max, possibleWords);
                    SwapTwoLetters(ref list[current], ref list[i]);
                }
        }

        public static void SwapTwoLetters(ref char a, ref char b)
        {
            char temp = a;
            a = b;
            b = temp;
        }
    }
}
