﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace CountdownLetters
{
    public class GameManager
    {
        private const string Vowel = "vowel";
        private const string Consonant = "consonant";

        private int _turnLimit;
        private int _roundLimit = 4;
        private int _maxVowels = 5;
        private int _maxConsonants = 6;

        private Stack<char> _consonants;
        private Stack<char> _vowels;
        private List<char> _selectedLetters;
        private int _roundCounter;
        private int _turnCounter;
        private int _scoreCounter;
        private int _vowelCounter;
        private int _consonantCounter;

        public GameManager(int turnLimit = 9,
                           int roundLimit = 4,
                           int maxVowels = 5,
                           int maxConsonants = 6)
        {
            _turnLimit = turnLimit;
            _roundLimit = roundLimit;
            _maxVowels = maxVowels;
            _maxConsonants = maxConsonants;
            _scoreCounter = 0;
            _roundCounter = 1;
        }


        public void Init()
        {
            Lexicon.LoadResource();
         //   List<List<int>>  test = Lexicon.GetInputCombinationsRec2(new List<char>() { 'a', 'b', 'c', 'd', 'a', 'b', 'c', 'd', 'd' }, 4, new List<List<int>>());
          //  Lexicon.GetLongestWordForletters(new List<char>() {  });
            
            SetupGame();
            StartGame();
        }

        private void StartGame()
        {
            while(_roundCounter <= _roundLimit)
            {
                Console.WriteLine($"Round {_roundCounter}");
                Console.WriteLine($"Current score {_scoreCounter}");

                while (_turnCounter <= _turnLimit)
                {
                    PerformTurn();
                }

                PerformGuessPhase();
                _roundCounter++;

                //Reset the game back to the base state
                SetupGame();
            }

            Console.WriteLine($"Game over. Your final score is: {_scoreCounter}");
            Console.WriteLine("Play again? y/n");
            string playAgain = Console.ReadLine();

            //Reset game-
            if(playAgain.ToLower() == "y" || playAgain.ToLower() == "yes")
            {
                _roundCounter = 1;
                _scoreCounter = 0;
                SetupGame();
                StartGame();
            }

        }

        //Let the player take a guess
        private void PerformGuessPhase()
        {
            Console.WriteLine($"Your final letters are: {string.Join(',', _selectedLetters)}");
            Console.WriteLine("You have 30 seconds to create the longest word you're able. Go!");
            Stopwatch sw = new Stopwatch();
            sw.Start();
            string guess = Console.ReadLine();
            sw.Stop();
            if(sw.Elapsed.Seconds > 30)
            {
                Console.WriteLine("Sorry you took too long to make your guess. Try to be faster next next.");
                Console.WriteLine("You scored 0 points!");
            }
            else
            {
                bool validInput = ValidateGuess(guess);
                if(validInput)
                {
                    bool wordExists = Lexicon.CheckDictionaryForWord(guess);
                    if(wordExists)
                    {
                        //Guess success case
                        _scoreCounter += guess.Length;
                        Console.WriteLine($"You scored {guess.Length} points!");
                        Console.WriteLine($"You now have {_scoreCounter} points.");
                    }
                    else
                    {
                        Console.WriteLine("Unfortuantely that word doesn't exist. Better luck next time.");
                    }
                }
                else
                {
                    Console.WriteLine("Unfortuantely the word you've entered is invalid");
                }
                Console.WriteLine($"The longest possible word for this round was {Lexicon.GetLongestWordForletters(_selectedLetters)}");
                
            }
        }

        private bool ValidateGuess(string guess)
        {
            //Check that the input is valid
            if (guess.Length > 0)
            {
                guess = guess.ToLower();
                List<char> lettersInGuessWord = guess.ToCharArray().ToList();
                foreach (char letter in _selectedLetters)
                {
                    var index = lettersInGuessWord.IndexOf(char.ToLower(letter));
                    if (index > -1)
                    {
                        lettersInGuessWord.RemoveAt(index);
                    }
                }
                if(lettersInGuessWord.Count > 0)
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            return true;

        }

        //peforms standard gameloop turn
        private void PerformTurn()
        {
            Console.WriteLine($"Your current letters are: {string.Join(',', _selectedLetters)}");
            Console.WriteLine("Vowel or Consanant?");
            string name = Console.ReadLine();

            if(name.ToLower() == Vowel || name.ToLower() == "v")
            {
                VowelSelected();
            }
            else if(name.ToLower() == Consonant || name.ToLower() == "c")
            {
                ConsonantSelected();
            }
            else
            {
                PerformTurn();
            }
        }

        private void ConsonantSelected()
        {
            if (_consonantCounter < _maxConsonants)
            {
                _selectedLetters.Add(_consonants.Pop());
                _turnCounter++;
                _consonantCounter++;
            }
            else
            {
                Console.WriteLine($"Please select at least {_turnLimit - _maxConsonants} vowels");
            }
        }

        private void VowelSelected()
        {
            if (_vowelCounter < _maxVowels)
            {
                _selectedLetters.Add(_vowels.Pop());
                _turnCounter++;
                _vowelCounter++;
            }
            else
            {
                Console.WriteLine($"Please select at least {_turnLimit - _maxVowels} consonants");
            }
        }

        private void SetupGame()
        {
            _vowelCounter = 0;
            _consonantCounter = 0;
            _turnCounter = 1;
            _selectedLetters = new List<char>();
            List<char> consonants = new List<char>();
            List<char>  vowels = new List<char>();

            //Create lists based on frequency
            foreach (string letter in Enum.GetNames(typeof(LetterFrequency)))
            {
                int quantity = (int)Enum.Parse(typeof(LetterFrequency), letter);

                if (letter == "A" ||
                    letter == "E" ||
                    letter == "I" ||
                    letter == "O" ||
                    letter == "U")
                {
                    AddLettersToSetupList(vowels, letter.ToLower().ToCharArray()[0], quantity);
                }
                else
                {
                    AddLettersToSetupList(consonants, letter.ToLower().ToCharArray()[0], quantity);
                }
            }


            //Randomize
            consonants = consonants.OrderBy(x => Guid.NewGuid()).ToList();
            vowels = vowels.OrderBy(x => Guid.NewGuid()).ToList();

            //create working stacks
            _consonants = new Stack<char>(consonants);
            _vowels = new Stack<char>(vowels);
        }

        private void AddLettersToSetupList(List<char> list, char letter, int quantity)
        {
            for (int i = 0; i < quantity; i++)
            {
                list.Add(letter);
            }
        }
    }
}
